package com.jen.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.jean.pojos.agenda;

@ManagedBean(name="Agenda")
@RequestScoped
public class agendas  {

	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("Conect");
    EntityManager em= emf.createEntityManager();
    
	private int id;
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	private String nombre;
	
	private int numero;

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	
	public List<agenda> getListaAgenda(){
		
		List<agenda> miLista = em.createNamedQuery("agenda.MostrarDatos",agenda.class).getResultList();
		System.out.println("Datos: "+miLista);
	    
	    return em.createNamedQuery("agenda.MostrarDatos",agenda.class).getResultList();
	    
	    
	    
	   
	}
	
	
}
