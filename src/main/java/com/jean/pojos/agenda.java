package com.jean.pojos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="Agenda")
@NamedQueries({
	
	@NamedQuery(name="agenda.MostrarDatos", query="select a from agenda a")
})

public class agenda implements Serializable{
		
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 8256713524587177190L;
	
	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="Nombre")
	private String nombre;
	
	@Column(name="numero")
	private int numero;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "agenda [id=" + id + ", nombre=" + nombre + ", numero=" + numero + "]";
	}
	
	
	
}
